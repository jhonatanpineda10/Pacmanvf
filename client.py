import pygame
import zmq
import random
import sys
from collections import namedtuple
import string
"""
Constantes globales
"""

#instalar ...
#conda install -c cogsci pygame
#sudo apt-get install libsdl-ttf2.0-0

# Colores
NEGRO = (0, 0, 0)
BLANCO = (255, 255, 255)
AZUL = (0, 0, 255)
ROJO = (255, 0, 0)
VERDE = (0,255,0)
AMARILLO = (255,255,0)
# Dimensiones de la pantalla
LARGO_PANTALLA  = 606
ALTO_PANTALLA = 606



class Pacman(pygame.sprite.Sprite):
    
    # Función Constructor
    def __init__(self, x, y,pantalla):
        #  Llama al constructor padre
        super().__init__()
        self.estado = True
        self.color = AMARILLO
        self.pantalla = pantalla
        # Establecemos el alto y largo
        self.image = pygame.Surface([35, 35])
        self.image.fill(self.color)
        # Establece como origen la esquina superior izquierda.
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x

        # Establecemos el vector velocidad
        self.cambio_x = 0
        self.cambio_y = 0
        self.paredes = None
        #self.listapacman = [] 

    def fantasma(self):
        self.estado = False
        self.image = pygame.Surface([35, 35])
        self.image.fill(BLANCO)
    
    def normal(self):
        self.estado = True
        self.image = pygame.Surface([35, 35])
        self.image.fill(AMARILLO)

    def quiensoy(self):
        pygame.font.init()
        font2 = pygame.font.Font("freesansbold.ttf", 24)#puntos
        text2=font2.render("yo", True, NEGRO)
        self.pantalla.blit(text2, [self.rect.x, self.rect.y])

    def cambiovelocidad(self, x, y):
        """ Cambia la velocidad del pacman. """
        self.cambio_x += x
        self.cambio_y += y

    def update(self):
        """ Cambia la velocidad del pacman. """
        # Desplazar izquierda/derecha
        self.rect.x += self.cambio_x

        # Hemos chocado contra la pared después de esta actualización?
        lista_impactos_bloques = pygame.sprite.spritecollide(self, self.paredes, False)
        for bloque in lista_impactos_bloques:
            #Si nos estamos desplazando hacia la derecha, hacemos que nuestro lado derecho sea el lado izquierdo del objeto que hemos tocado-
            if self.cambio_x > 0:
                self.rect.right = bloque.rect.left
            else:
                # En caso contrario, si nos desplazamos hacia la izquierda, hacemos lo opuesto.
                self.rect.left = bloque.rect.right

        self.rect.y += self.cambio_y

        # Comprobamos si hemos chocado contra algo
        lista_impactos_bloques = pygame.sprite.spritecollide(self, self.paredes, False)
        for bloque in lista_impactos_bloques:

            # Reseteamos nuestra posición basándonos en la parte superior/inferior del objeto.
            if self.cambio_y > 0:
                self.rect.bottom = bloque.rect.top
            else:
                self.rect.top = bloque.rect.bottom
        # Comprobamos si hemos chocado contra algo


class Pared(pygame.sprite.Sprite):
    """ Pared con la que el pacman puede encontrarse. """
    def __init__(self, x, y, largo, alto):
        """ Constructor para la pared con la que el pacman puede encontrarse """
        #  Llama al constructor padre
        super().__init__()

        # Construye una pared azul con las dimensiones especificadas por los parámetros
        self.image = pygame.Surface([largo, alto])
        self.image.fill(AZUL)

        # Establece como origen la esquina superior izquierda.
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x

class Galleta(pygame.sprite.Sprite):

    def __init__(self, color, largo, alto):
        # Llama al constructor de la clase padre (Sprite)
        super().__init__()
        self.image = pygame.Surface([largo, alto])
        self.image.fill(color)

        # Obtenemos el objeto rectángulo que posee las dimensiones de la imagen
        # Actualizamos la posición de ese objeto estableciendo los valores para
        # rect.x y rect.y
        self.rect = self.image.get_rect()

class Super_Galleta(pygame.sprite.Sprite):

    super_lista = None
    def __init__(self):
        self.super_lista = pygame.sprite.Group()

    def llenar(self):
        galleta1 = Galleta(VERDE, 30, 30)
        galleta1.rect.x = 20
        galleta1.rect.y = 20
        self.super_lista.add(galleta1)
        galleta2 = Galleta(VERDE, 30, 30)
        galleta2.rect.x = 20
        galleta2.rect.y = 560
        self.super_lista.add(galleta2)
        galleta3 = Galleta(VERDE, 30, 30)
        galleta3.rect.x = 560
        galleta3.rect.y = 20
        self.super_lista.add(galleta3)
        galleta4 = Galleta(VERDE, 30, 30)
        galleta4.rect.x = 560
        galleta4.rect.y = 560
        self.super_lista.add(galleta4)
        return self.super_lista

def main():   
    pygame.init()

    pygame.font.init()

    font = pygame.font.Font("freesansbold.ttf", 50)#para iniciar el juego

    font2 = pygame.font.Font("freesansbold.ttf", 24)#puntos

    font3 = pygame.font.Font("freesansbold.ttf", 70)#para iniciar el juego
    # Creamos una pantalla 
    pantalla = pygame.display.set_mode([LARGO_PANTALLA, ALTO_PANTALLA])

    # Creamos el título de la ventana
    pygame.display.set_caption('Pacman')


    listade_todoslos_sprites = pygame.sprite.Group()# Lista que almacena todos los sprites
    pared_lista = pygame.sprite.Group()
    galleta_lista = pygame.sprite.Group()
    supergalleta_lista = pygame.sprite.Group()

    pacman_lista = pygame.sprite.Group()
    pacman2_lista = pygame.sprite.Group()
    pacman3_lista = pygame.sprite.Group()
    pacman4_lista = pygame.sprite.Group()
    pacman5_lista = pygame.sprite.Group()
    pacman6_lista = pygame.sprite.Group()

    fantasmaslista1 = pygame.sprite.Group()
    fantasmaslista2 = pygame.sprite.Group()
    fantasmaslista3 = pygame.sprite.Group()
    fantasmaslista4 = pygame.sprite.Group()
    fantasmaslista5 = pygame.sprite.Group()

    paredes = [ [0,0,6,600],
                [0,0,600,6],
                [0,600,606,6],
                [600,0,6,606],
                #[300,0,6,66],
                #[60,60,186,6],
                #[360,60,186,6],
                #[60,120,66,6],
                [60,120,6,126],
                [180,120,246,6],
                [300,120,6,66],
                [300,360,6,120],
                #[480,120,66,6],
                [540,120,6,126],
                #[120,180,126,6],
                #[120,180,6,126],
                #[360,180,126,6],
                #[480,180,6,126],
                [180,240,6,126],
                [180,360,246,6],
                [420,240,6,126],
                #[240,240,42,6],
                #[324,240,42,6],
                #[240,240,6,66],
                #[240,300,126,6],
                #[360,240,6,66],
                #[0,300,66,6],
                #[540,300,66,6],
                #[60,360,66,6],
                [60,360,6,186],
                #[480,360,66,6],
                [540,360,6,186],
                #[120,420,366,6],
                #[120,420,6,66],
                #[480,420,6,66],
                [180,480,246,6],
                #[300,480,6,66],
                [120,540,126,6],
                [360,540,126,6]
                ]
    for item in paredes:
        pared = Pared(item[0],item[1],item[2],item[3])
        pared_lista.add(pared)
        listade_todoslos_sprites.add(pared)

    pacman = Pacman(-100,-100,pantalla)
    pacman.paredes = pared_lista
    listade_todoslos_sprites.add(pacman)


    pacman2 = Pacman(-100,-100,pantalla)
    pacman2.paredes = pared_lista
    listade_todoslos_sprites.add(pacman2)

    pacman3 = Pacman(-100, -100,pantalla)
    pacman3.paredes = pared_lista
    listade_todoslos_sprites.add(pacman3)

    pacman4 = Pacman(-100, -100,pantalla)
    pacman4.paredes = pared_lista
    listade_todoslos_sprites.add(pacman4)

    pacman5 = Pacman(-100, -100,pantalla)
    pacman5.paredes = pared_lista
    listade_todoslos_sprites.add(pacman5)

    pacman6 = Pacman(-100, -100,pantalla)
    pacman6.paredes = pared_lista
    listade_todoslos_sprites.add(pacman6)

    supergalleta_lista = Super_Galleta().llenar()

    for row in range(19):
        for column in range(19):
            bloque = Galleta(ROJO, 15,15)
            bloque.rect.x = (60*column+6)+20
            bloque.rect.y = (60*row+6)+20
            b_collide = pygame.sprite.spritecollide(bloque, pared_lista, False)
            s_collide = pygame.sprite.spritecollide(bloque, supergalleta_lista, False)
            if b_collide:
                continue
            if s_collide:
                continue
            else:
                galleta_lista.add(bloque)
                listade_todoslos_sprites.add(bloque)

    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.connect('tcp://localhost:4000')
    poll = zmq.Poller()
    poll.register(socket, zmq.POLLIN)
    poll.register(pygame.K_LEFT, zmq.POLLIN)
    poll.register(pygame.K_RIGHT, zmq.POLLIN)
    poll.register(pygame.K_UP, zmq.POLLIN)
    poll.register(pygame.K_DOWN, zmq.POLLIN)

    '''
    pacmanposiciones = [ CLIENTE 1   [CLIENTE1, PACMAN3,  PACMAN4,  PACMAN5, PACMAN6]
                         CLIENTE 2   [PACMAN2,  CLIENTE2, PACMAN4,  PACMAN5, PACMAN6]
                         CLIENTE 3   [PACMAN2,  PACMAN3,  CLIENTE3, PACMAN5, PACMAN6]
                         CLIENTE 4   [PACMAN2,  PACMAN3,  PACMAN4,  CLIENTE4,PACMAN6]
                         CLIENTE 5   [PACMAN2,  PACMAN3,  PACMAN4,  PACMAN5, CLIENTE5] '''

    reloj = pygame.time.Clock()
    salir = False
    marcador = 0 # puntos
    pacident = 0 # Quien es el pacman que se mueve
    ini = True # iniciar el juego
    end = False # fin del juego
    cont = [100,100,100,100,100,100] 
    cambio = [0,0,0,0,0,0]
    fant = [0,0,0,0,0,0]
    validar = [0,0,0,0,0,0]

    while not salir:
    # Validar el tiempo
        if validar[0] == 1:
            cont[0]+=1
        if validar[1] == 1:
            cont[1]+=1
        if validar[2] == 1:
            cont[2]+=1
        if validar[3] == 1:
            cont[3]+=1
        if validar[4] == 1:
            cont[4]+=1 
        if validar[5] == 1:
            cont[5]+=1

        supergalleta_lista = Super_Galleta().llenar()
        ################################################
        if fant[0] == 1: #Cliente en el que estoy
            pacman2.fantasma()
            pacman3.fantasma()
            pacman4.fantasma()
            pacman5.fantasma()
            pacman6.fantasma()
            cambio[0] = 1
            fant[0] = 0
            validar[0] = 1

        if cambio[0] == 1 and cont[0] == 500:
            pacman2.normal()
            pacman3.normal()
            pacman4.normal()
            pacman5.normal()
            pacman6.normal()
            cont[0] = 0
            validar[0] = 0
            cambio[0] = 0

        ######cLIENTE 1##############
        if fant[1] == 1:
            pacman.fantasma()
            pacman3.fantasma()
            pacman4.fantasma()
            pacman5.fantasma()
            pacman6.fantasma()
            cambio[1] = 1
            fant[1] = 0
            validar[1] = 1
        
        if cambio[1] == 1 and cont[1] == 500:
            pacman.normal()
            pacman3.normal()
            pacman4.normal()
            pacman5.normal()
            pacman6.normal()
            cont[1] = 0
            validar[1] = 0
            cambio[1] = 0
        ######cLIENTE 2 ##############
        if fant[2] == 1:
            pacman.fantasma()
            pacman2.fantasma()
            pacman4.fantasma()
            pacman5.fantasma()
            pacman6.fantasma()
            cambio[2] = 1
            fant[2] = 0
            validar[2] = 1
        
        if cambio[2] == 1 and cont[2] == 500:
            pacman.normal()
            pacman2.normal()
            pacman4.normal()
            pacman5.normal()
            pacman6.normal()
            cont[2] = 0
            validar[2] = 0
            cambio[2] = 0
        ######cLIENTE 3 ##############
        if fant[3] == 1:
            pacman.fantasma()
            pacman2.fantasma()
            pacman3.fantasma()
            pacman5.fantasma()
            pacman6.fantasma()
            cambio[3] = 1
            fant[3] = 0
            validar[3] = 1
        
        if cambio[3] == 1 and cont[3] == 500:
            pacman.normal()
            pacman2.normal()
            pacman3.normal()
            pacman5.normal()
            pacman6.normal()
            cont[3] = 0
            validar[3] = 0
            cambio[3] = 0
        ######cLIENTE 4 ##############
        if fant[4] == 1:
            pacman.fantasma()
            pacman2.fantasma()
            pacman3.fantasma()
            pacman4.fantasma()
            pacman6.fantasma()
            cambio[4] = 1
            fant[4] = 0
            validar[4] = 1
        
        if cambio[4] == 1 and cont[4] == 500:
            pacman.normal()
            pacman2.normal()
            pacman3.normal()
            pacman4.normal()
            pacman6.normal()
            cont[4] = 0
            validar[4] = 0
            cambio[4] = 0
        ######cLIENTE 5 ##############
        if fant[5] == 1:
            pacman.fantasma()
            pacman2.fantasma()
            pacman3.fantasma()
            pacman4.fantasma()
            pacman5.fantasma()
            cambio[5] = 1
            fant[5] = 0
            validar[5] = 1
        
        if cambio[5] == 1 and cont[5] == 500:
            pacman.normal()
            pacman2.normal()
            pacman3.normal()
            pacman4.normal()
            pacman5.normal()
            cont[5] = 0
            validar[5] = 0
            cambio[5] = 0
        ###############################################
        sockets = dict(poll.poll())
        if socket in sockets:
            if sockets[socket] == zmq.POLLIN:
                m = socket.recv_multipart()
                a = ([bytes(str("200"),'ascii')]) #mensaje
                b = ([bytes(str("380"),'ascii')]) #mensaje
                c = ([bytes(str("290"),'ascii')]) #mensaje
                d = ([bytes(str("201"),'ascii')]) #mensaje
                e = ([bytes(str("381"),'ascii')]) #mensaje
                ############# CLIENTE 1 ########################
                if m == a:
                    pacident = 200
                    # dibujar pacman en el tablero
                    pacman.rect.x =200
                    pacman.rect.y =320
                    pacman3.rect.x =380
                    pacman3.rect.y =320
                    pacman4.rect.x =290
                    pacman4.rect.y =320
                    pacman5.rect.x =201
                    pacman5.rect.y =200
                    pacman6.rect.x =381
                    pacman6.rect.y =200

                    # agregar los otros pacman al tablero del cliente actual para la colision
                    pacman_lista.add(pacman3)
                    pacman_lista.add(pacman4)
                    pacman_lista.add(pacman5)
                    pacman_lista.add(pacman6)

                    #colision para el pacman 3 en todos los clientes
                    pacman3_lista.add(pacman4)
                    pacman3_lista.add(pacman5)
                    pacman3_lista.add(pacman6)

                    #colision para el pacman 4 en todos los clientes
                    pacman4_lista.add(pacman3)
                    pacman4_lista.add(pacman5)
                    pacman4_lista.add(pacman6)

                    #colision para el pacman 5 en todos los clientes
                    pacman5_lista.add(pacman3)
                    pacman5_lista.add(pacman4)
                    pacman5_lista.add(pacman6)

                    #colision para el pacman 6 en todos los clientes
                    pacman6_lista.add(pacman3)
                    pacman6_lista.add(pacman4)
                    pacman6_lista.add(pacman5)

                    fantasmaslista2.add(pacman)
                    fantasmaslista3.add(pacman)
                    fantasmaslista4.add(pacman)
                    fantasmaslista5.add(pacman)              
                ############# CLIENTE 2 ########################
                elif m == b:
                    pacident = 380

                    pacman.rect.x =380
                    pacman.rect.y =320
                    pacman2.rect.x =200
                    pacman2.rect.y =320
                    pacman4.rect.x =290
                    pacman4.rect.y =320
                    pacman5.rect.x =201
                    pacman5.rect.y =200
                    pacman6.rect.x =381
                    pacman6.rect.y =200

                    pacman_lista.add(pacman2)
                    pacman_lista.add(pacman4)
                    pacman_lista.add(pacman5)
                    pacman_lista.add(pacman6)

                    #colision para el pacman 2 en todos los clientes
                    pacman2_lista.add(pacman4)
                    pacman2_lista.add(pacman5)
                    pacman2_lista.add(pacman6)

                    #colision para el pacman 4 en todos los clientes
                    pacman4_lista.add(pacman2)
                    pacman4_lista.add(pacman5)
                    pacman4_lista.add(pacman6)

                    #colision para el pacman 5 en todos los clientes
                    pacman5_lista.add(pacman2)
                    pacman5_lista.add(pacman4)
                    pacman5_lista.add(pacman6)

                    #colision para el pacman 6 en todos los clientes
                    pacman6_lista.add(pacman2)
                    pacman6_lista.add(pacman4)
                    pacman6_lista.add(pacman5)

                    fantasmaslista1.add(pacman)
                    fantasmaslista3.add(pacman)
                    fantasmaslista4.add(pacman)
                    fantasmaslista5.add(pacman)    
                ############# CLIENTE 3 ########################

                elif m == c:
                    pacident = 290

                    pacman.rect.x =290
                    pacman.rect.y =320
                    pacman2.rect.x =200
                    pacman2.rect.y =320
                    pacman3.rect.x =380
                    pacman3.rect.y =320
                    pacman5.rect.x =201
                    pacman5.rect.y =200
                    pacman6.rect.x =381
                    pacman6.rect.y =200

                    pacman_lista.add(pacman2)
                    pacman_lista.add(pacman3)
                    pacman_lista.add(pacman5)  
                    pacman_lista.add(pacman6)

                    #colision para el pacman 2 en todos los clientes
                    pacman2_lista.add(pacman3)
                    pacman2_lista.add(pacman5)
                    pacman2_lista.add(pacman6)

                    #colision para el pacman 3 en todos los clientes
                    pacman3_lista.add(pacman2)
                    pacman3_lista.add(pacman5)
                    pacman3_lista.add(pacman6)

                    #colision para el pacman 5 en todos los clientes
                    pacman5_lista.add(pacman2)
                    pacman5_lista.add(pacman3)
                    pacman5_lista.add(pacman6)

                    #colision para el pacman 6 en todos los clientes
                    pacman6_lista.add(pacman2)
                    pacman6_lista.add(pacman3)
                    pacman6_lista.add(pacman5)

                    fantasmaslista1.add(pacman)
                    fantasmaslista2.add(pacman)
                    fantasmaslista4.add(pacman)
                    fantasmaslista5.add(pacman)  
                ############# CLIENTE 4 ########################
                elif m == d:
                    pacident = 201

                    pacman.rect.x =201
                    pacman.rect.y =200
                    pacman2.rect.x =200
                    pacman2.rect.y =320
                    pacman3.rect.x =380
                    pacman3.rect.y =320
                    pacman4.rect.x =290
                    pacman4.rect.y =320
                    pacman6.rect.x =381
                    pacman6.rect.y =200  

                    pacman_lista.add(pacman2)                
                    pacman_lista.add(pacman3)
                    pacman_lista.add(pacman4)
                    pacman_lista.add(pacman6)

                    #colision para el pacman 2 en todos los clientes
                    pacman2_lista.add(pacman3)
                    pacman2_lista.add(pacman4)
                    pacman2_lista.add(pacman6)

                    #colision para el pacman 3 en todos los clientes
                    pacman3_lista.add(pacman2)
                    pacman3_lista.add(pacman4)
                    pacman3_lista.add(pacman6)

                    #colision para el pacman 4 en todos los clientes
                    pacman4_lista.add(pacman2)
                    pacman4_lista.add(pacman3)
                    pacman4_lista.add(pacman6)

                    #colision para el pacman 6 en todos los clientes
                    pacman6_lista.add(pacman2)
                    pacman6_lista.add(pacman3)
                    pacman6_lista.add(pacman4)

                    fantasmaslista1.add(pacman)
                    fantasmaslista2.add(pacman)
                    fantasmaslista3.add(pacman)
                    fantasmaslista5.add(pacman)  
                ############# CLIENTE 5 ########################        
                elif m == e:
                    pacident = 381

                    pacman.rect.x =381
                    pacman.rect.y =200
                    pacman2.rect.x =200
                    pacman2.rect.y =320
                    pacman3.rect.x =380
                    pacman3.rect.y =320
                    pacman4.rect.x =290
                    pacman4.rect.y =320
                    pacman5.rect.x =201
                    pacman5.rect.y =200
              
                    pacman_lista.add(pacman2) 
                    pacman_lista.add(pacman3)
                    pacman_lista.add(pacman4)
                    pacman_lista.add(pacman5)

                    #colision para el pacman 3 en todos los clientes
                    pacman3_lista.add(pacman2)
                    pacman3_lista.add(pacman4)
                    pacman3_lista.add(pacman5)

                    #colision para el pacman 4 en todos los clientes
                    pacman4_lista.add(pacman2)
                    pacman4_lista.add(pacman3)
                    pacman4_lista.add(pacman5)

                    #colision para el pacman 5 en todos los clientes
                    pacman5_lista.add(pacman2)
                    pacman5_lista.add(pacman3)
                    pacman5_lista.add(pacman4)

                    #colision para el pacman 2 en todos los clientes
                    pacman2_lista.add(pacman3)
                    pacman2_lista.add(pacman4)
                    pacman2_lista.add(pacman5)

                    fantasmaslista1.add(pacman)
                    fantasmaslista2.add(pacman)
                    fantasmaslista3.add(pacman)  
                    fantasmaslista4.add(pacman)
                else:
                    g = str(m)
                    nx,pos,ny = g.split(",")
                    nx = nx.split("[b'")
                    ny = ny.split("']")
                    nx = int(nx[1])
                    ny = int(ny[0])
                    if int(pos) == 200:
                        pacman2.cambiovelocidad(nx,ny)
                    elif int(pos) == 380:
                        pacman3.cambiovelocidad(nx,ny)
                    elif int(pos) == 290:
                        pacman4.cambiovelocidad(nx,ny)
                    elif int(pos) == 201:
                        pacman5.cambiovelocidad(nx,ny)
                    elif int(pos) == 381:
                        pacman6.cambiovelocidad(nx,ny)


        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                salir = True

            if evento.type == pygame.KEYDOWN:
                if evento.key == pygame.K_x and ini:
                    pospacman = (str(0)+","+str(pacident)+","+str(0))
                    socket.send_multipart([bytes(str(pospacman), 'ascii')])
                    ini = False
                if evento.key == pygame.K_LEFT:
                    pacman.cambiovelocidad(-3,0)
                    pospacman = (str(-3)+","+str(pacident)+","+str(0))
                    socket.send_multipart([bytes(str(pospacman), 'ascii')])
                elif evento.key == pygame.K_RIGHT:
                    pacman.cambiovelocidad(3,0)
                    pospacman = (str(3)+","+str(pacident)+","+str(0))
                    socket.send_multipart([bytes(str(pospacman), 'ascii')])
                elif evento.key == pygame.K_UP:
                    pacman.cambiovelocidad(0,-3)
                    pospacman = (str(0)+","+str(pacident)+","+str(-3))
                    socket.send_multipart([bytes(str(pospacman), 'ascii')])
                elif evento.key == pygame.K_DOWN:
                    pacman.cambiovelocidad(0,3)
                    pospacman = (str(0)+","+str(pacident)+","+str(3))
                    socket.send_multipart([bytes(str(pospacman), 'ascii')])

            elif evento.type == pygame.KEYUP:
                if evento.key == pygame.K_LEFT:
                    pacman.cambiovelocidad(3,0)
                    pospacman = (str(3)+","+str(pacident)+","+str(0))
                    socket.send_multipart([bytes(str(pospacman), 'ascii')])
                elif evento.key == pygame.K_RIGHT:
                    pacman.cambiovelocidad(-3,0)
                    pospacman = (str(-3)+","+str(pacident)+","+str(0))
                    socket.send_multipart([bytes(str(pospacman), 'ascii')])
                elif evento.key == pygame.K_UP:
                    pacman.cambiovelocidad(0,3)
                    pospacman = (str(0)+","+str(pacident)+","+str(3))
                    socket.send_multipart([bytes(str(pospacman), 'ascii')])
                elif evento.key == pygame.K_DOWN:
                    pacman.cambiovelocidad(0,-3)
                    pospacman = (str(0)+","+str(pacident)+","+str(-3))
                    socket.send_multipart([bytes(str(pospacman), 'ascii')])

        if pacman.estado:
            lista_impactos_galletas = pygame.sprite.spritecollide(pacman, galleta_lista, True)
            lista_impactos_super = pygame.sprite.spritecollide(pacman, supergalleta_lista, True)
            for galleta in lista_impactos_galletas:
                marcador += 1
            for supergalleta in lista_impactos_super: 
                fant[0] = 1

        if pacman2.estado:
            lista_impactos_galletas2 = pygame.sprite.spritecollide(pacman2, galleta_lista, True)
            lista_impactos_super2 = pygame.sprite.spritecollide(pacman2, supergalleta_lista, True)
            
            for supergalleta in lista_impactos_super2:
                fant[1] = 1

        if pacman3.estado:
            lista_impactos_galletas3 = pygame.sprite.spritecollide(pacman3, galleta_lista, True)
            lista_impactos_super3 = pygame.sprite.spritecollide(pacman3, supergalleta_lista, True)
            for supergalleta in lista_impactos_super3: 
                fant[2] = 1

        if pacman4.estado:
            lista_impactos_galletas4 = pygame.sprite.spritecollide(pacman4, galleta_lista, True)
            lista_impactos_super4 = pygame.sprite.spritecollide(pacman4, supergalleta_lista, True)
            for supergalleta in lista_impactos_super4:
                fant[3] = 1

        if pacman5.estado:
            lista_impactos_galletas5 = pygame.sprite.spritecollide(pacman5, galleta_lista, True)
            lista_impactos_super5 = pygame.sprite.spritecollide(pacman5, supergalleta_lista, True)
            for supergalleta in lista_impactos_super5:
                fant[4] = 1  
            
        if pacman6.estado:
            lista_impactos_galletas6 = pygame.sprite.spritecollide(pacman6, galleta_lista, True)
            lista_impactos_super6 = pygame.sprite.spritecollide(pacman6, supergalleta_lista, True)
            for supergalleta in lista_impactos_super6:
                fant[5] = 1     

        if cambio[0] == 1:
            lista_choquepacman = pygame.sprite.spritecollide(pacman, pacman_lista, True)
            for fantas in lista_choquepacman:
                marcador += 10

        if cambio[1] == 1:
            lista_choquepacman = pygame.sprite.spritecollide(pacman2,fantasmaslista1 , True)
            lista_choquepacman2 = pygame.sprite.spritecollide(pacman2,pacman2_lista, True)
            for fantas in lista_choquepacman:
                end = True

        if cambio[2] == 1:
            lista_choquepacman = pygame.sprite.spritecollide(pacman3,fantasmaslista2 , True)
            lista_choquepacman2 = pygame.sprite.spritecollide(pacman3,pacman3_lista, True)
            for fantas in lista_choquepacman:
                end = True
                

        if cambio[3] == 1:
            lista_choquepacman = pygame.sprite.spritecollide(pacman4,fantasmaslista3 , True)
            lista_choquepacman2 = pygame.sprite.spritecollide(pacman4,pacman4_lista, True)
            for fantas in lista_choquepacman:
                end = True
               

        if cambio[4] == 1:
            lista_choquepacman = pygame.sprite.spritecollide(pacman5,fantasmaslista4 , True)
            lista_choquepacman2 = pygame.sprite.spritecollide(pacman5,pacman5_lista, True)
            for fantas in lista_choquepacman:
                end = True
              

        if cambio[5] == 1:
            lista_choquepacman = pygame.sprite.spritecollide(pacman6,fantasmaslista5 , True)
            lista_choquepacman2 = pygame.sprite.spritecollide(pacman6,pacman6_lista, True)
            for fantas in lista_choquepacman:
                end = True
                            

        listade_todoslos_sprites.update()

        pantalla.fill(NEGRO)

        supergalleta_lista.draw(pantalla)

        listade_todoslos_sprites.draw(pantalla)

        pacman.quiensoy()
        
        #texto del juego
        if ini:
            text=font.render("Presiona x para iniciar", True, BLANCO)
            pantalla.blit(text, [30, 303])

        if end:
            pantalla.fill(NEGRO)
            text4=font3.render("FIN DEL JUEGO", True, BLANCO)
            pantalla.blit(text4, [50, 303])

        if len(pacman_lista) == 0 and not(ini):
            pantalla.fill(NEGRO)
            text4=font3.render("GANASTE", True, BLANCO)
            pantalla.blit(text4, [100, 303])

        text2=font2.render("PUNTOS: "+str(marcador), True, BLANCO)
        pantalla.blit(text2, [10, 10])
        pygame.display.flip()

        reloj.tick(60)

    pygame.quit()

if __name__ == '__main__':
    main()
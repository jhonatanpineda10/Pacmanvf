import zmq
import sys
from collections import namedtuple


def difusion(jugadores,socket,msg):
    for ident in jugadores:
        socket.send_multipart([ident, msg])

def main(): 
    context = zmq.Context()
    socket = context.socket(zmq.ROUTER)
    socket.bind('tcp://*:4000')
    print("Started server")
    a = 0
    x = 0

    todoslosjugadores = []
    nueva_lista = []
    pos = [200,380,290,201,381]
    while True:            
        ident, msg = socket.recv_multipart()
        if ident not in todoslosjugadores:
            print("seccion = " + str(x+1))
            todoslosjugadores.append(ident)
            socket.send_multipart([ident,bytes(str(pos[a]),'ascii')])
            a+=1
            if a == 5:
                a = 0
                x+=1                
        else:
            nueva_lista = [todoslosjugadores[i:i+5] for i in range(0, len(todoslosjugadores),5)]
            for key in nueva_lista:
                difusion(key,socket,msg)

if __name__ == '__main__':
    main()